//
//  ViewController.swift
//  FaceIt
//
//  Created by Michel Deiman on 27/02/2017.
//  Copyright © 2017 Michel Deiman. All rights reserved.
//

import UIKit

class EmojiViewController: UIViewController {
    
    @IBOutlet weak var faceView : FaceView!{
        didSet{
            updateUI()
            let pinshGesture = UIPinchGestureRecognizer(target: faceView, action:#selector (faceView.changeScaleReactingTo(pinchGesture:)))
            faceView.addGestureRecognizer(pinshGesture)
            let tapGesture = UITapGestureRecognizer(target: self, action:#selector (self.shakeHead))
            faceView.addGestureRecognizer(tapGesture)
            
            let upGesture = UISwipeGestureRecognizer(target: self, action:#selector (self.increaseHappienes))
            upGesture.direction = .up
            faceView.addGestureRecognizer(upGesture)
            
            let downGesture = UISwipeGestureRecognizer(target: self, action:#selector (self.decreaseHappienes))
            downGesture.direction = .down
            faceView.addGestureRecognizer(downGesture)

        }
    }
    
    var expression = FacialExpression(eyes: .open , mouth: .frown)
    {
        didSet{
            updateUI()
        }
    }
    func  increaseHappienes ()  {
        expression = expression.happier
    }
    func  decreaseHappienes ()  {
        expression = expression.sadder
    }
    func toggleEye( tapGesture: UITapGestureRecognizer)
    {
        if( tapGesture.state == .ended)
        {
            let eyesState : FacialExpression.Eyes = (expression.eyes == .open ) ? .closed : .open
            expression = FacialExpression(eyes: eyesState , mouth: expression.mouth)
        }
    }
    private struct HeadShake {
        static let angle = CGFloat.pi/6
        static let segmentDuration = 0.5
    }
    func shakeHead()
    {
        UIView.animate(withDuration: HeadShake.segmentDuration, animations: {
        self.faceView.transform = self.faceView.transform.rotated(by: HeadShake.angle)
        }) { (finished) in
            if ( finished ){
                UIView.animate(withDuration: HeadShake.segmentDuration, animations: {
                     self.view.transform = self.view.transform.rotated(by: -2 * HeadShake.angle)
                }, completion: { (finished) in
                    UIView.animate(withDuration: HeadShake.segmentDuration, animations:{
                        self.faceView.transform = self.faceView.transform.rotated(by: HeadShake.angle)
                    })
                })
            }
        }
    }
    private func updateUI()
    {
        switch expression.eyes {
        case .open:
            faceView?.eyesOpen = true
        case .closed:
            faceView?.eyesOpen = false
        case .squinting:
            faceView?.eyesOpen = false
        }
        faceView?.mouthCurvature = mouthCuratures[expression.mouth] ?? 0.0
    }
    private let mouthCuratures = [FacialExpression.Mouth.grin :0.5,
                                  .frown:-1.0,.neutral:0.0 ,.smile:1.0 , .smirk:-0.5]
    
}

