//
//  EmojiSelectorViewController.swift
//  FaceIt
//
//  Created by User on 1/13/18.
//  Copyright © 2018 Michel Deiman. All rights reserved.
//

import UIKit

class EmojiSelectorViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    var dest = segue.destination
        if let navControllerDest = dest as? UINavigationController
        {
            dest = navControllerDest.visibleViewController ?? dest
        }
        if let EmojiDest = dest as? EmojiViewController {
            if let experssionFromBtn = expressionDic[segue.identifier!]{
                EmojiDest.expression = experssionFromBtn
                EmojiDest.navigationItem.title = ( sender as? UIButton )?.currentTitle
            }
        }
        
    }
    
    var expressionDic : Dictionary<String,FacialExpression>  = [
    "happy" : FacialExpression(eyes: .open , mouth: .smile),
    "sad": FacialExpression(eyes: .closed , mouth: .frown),
    "worried": FacialExpression(eyes: .open , mouth: .smirk )
    ]
}
