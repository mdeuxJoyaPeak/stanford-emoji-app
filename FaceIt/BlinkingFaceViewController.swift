//
//  BlinkingFaceViewController.swift
//  FaceIt
//
//  Created by MacBook Pro on 1/18/18.
//  Copyright © 2018 Michel Deiman. All rights reserved.
//

import UIKit

class BlinkingFaceViewController: EmojiViewController {
    
    var blinking = false {
        didSet{
            
            blink()
            
        }
    }
    
    struct BlinkingConstansts {
        static let openInterval: TimeInterval = 0.4
        static let closingInterval: TimeInterval = 2.5
    }
    func blink ()
    {
        if(blinking)
        {
            faceView.eyesOpen = false
            Timer.scheduledTimer(withTimeInterval: BlinkingConstansts.openInterval, repeats: false, block: { [weak self] timer in
                self?.faceView.eyesOpen = true
                
                Timer.scheduledTimer(withTimeInterval: BlinkingConstansts.closingInterval, repeats: false , block:{ [weak self] timer in
                    self?.blink()
                } )
            })
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        blinking = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
}
